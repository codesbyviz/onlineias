import React, { Component } from 'react';
import { View,Dimensions,RefreshControl,Modal,StyleSheet,BackHandler, Image, ScrollView } from 'react-native';
import {WebView} from 'react-native-webview';
import { SafeAreaConsumer, SafeAreaProvider } from 'react-native-safe-area-context';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.WEBVIEW_REF = React.createRef();
    this.state = {
        loading:false,
        backButtonEnabled:false,
        webviewref:null,
        screenSize:Dimensions.get('screen')
    };
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener("hardwareBackPress",this.backHandler);
  }
  backHandler = () => {
    if(this.state.backButtonEnabled) {
        this.WEBVIEW_REF.current.goBack();
        return true;
    }
}
  componentWillUnmount() {
    this.backHandler.remove();
  }  
  handleWebViewNavigationStateChange = newNavState => {
      this.setState({loading:newNavState.loading,backButtonEnabled: newNavState.canGoBack});
  }
  onRefresh = async () => {
    await this.WEBVIEW_REF.current.reload();
  }
  render() {
    let appUrl = "http://onlineias.com/application_ui";
    return (
      <SafeAreaProvider>
          <Modal animationType="fade" transparent={true} visible={this.state.loading} 
          onRequestClose={() => {}}>
            <View style={styles.modal}>
              <Image style={styles.loadingimage} source={require("./assets/logo.png")}/>
            </View>
          </Modal>
        <SafeAreaConsumer>{insets => 
            <WebView style={{marginTop:insets.top}} ref={this.WEBVIEW_REF}
            originWhitelist={['*']}
            source={{ uri: appUrl }}
            onNavigationStateChange={this.handleWebViewNavigationStateChange}/>
            }</SafeAreaConsumer>         
      </SafeAreaProvider>
    );
  }
}

const styles = StyleSheet.create({
    main:{
        minWidth: Dimensions.get('screen').width,
        minHeight: Dimensions.get('screen').height,
    },
    modal:{
        backgroundColor:"#000000",
        opacity:0.8,
        flex: 1, 
        alignItems: 'center', 
        justifyContent: 'center',
    },
    loadingimage:{
        borderRadius:200,
        height:80,
        width:80,
    }
  });